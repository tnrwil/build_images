# Build_Images

This will be the base and pipeline to build images for gitlab runners

[How to build images](https://cuteprogramming.wordpress.com/2017/12/13/create-a-docker-image-from-centos-minimal-iso/)

[HashiCorp Packer in Container](https://hub.docker.com/r/hashicorp/packer/tags)

[Build Using ISO](https://codeblog.dotsandbrackets.com/build-image-packer/)

[Build using docker and ISO](https://cuteprogramming.wordpress.com/2017/12/13/create-a-docker-image-from-centos-minimal-iso/)

[this worked for creating docker images](https://cuteprogramming.wordpress.com/2017/12/13/create-a-docker-image-from-centos-minimal-iso/)

# Creating Contexts

[Creating Contexts for Kubernetes Cluster](http://docs.shippable.com/deploy/tutorial/create-kubeconfig-for-self-hosted-kubernetes-cluster/)
